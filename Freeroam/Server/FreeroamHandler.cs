﻿using System;
using System.Threading.Tasks;
using AltV.Net;
using AltV.Net.Async;
using AltV.Net.Data;
using AltV.Net.Elements.Entities;
using AltV.Net.Enums;

namespace Server
{
    public class FreeroamHandler : Resource
    {
        public override void OnStart()
        {
            Alt.OnPlayerConnect += PlayerConnect;
            Alt.OnPlayerEvent += AltOnOnPlayerEvent;
        }

        private void AltOnOnPlayerEvent(IPlayer player, string eventname, object[] args)
        {
            switch (eventname)
            {
                case "chatmessage":
                    ChatMessage(player, args);
                    break;
            }
        }

        private void ChatMessage(IPlayer player, object[] args)
        {
            player.Emit("chatmessage", args[0]);

            string chatMessage = args[0].ToString();

            if (string.IsNullOrEmpty(chatMessage))
                return;

            if (chatMessage.Length > 0 && chatMessage.StartsWith("/"))
            {
                string[] commandArgs = chatMessage.Substring(1, chatMessage.Length - 1).Split(' ');

                switch (commandArgs[0])
                {
                    case "veh":
                        IVehicle vehicle = Alt.CreateVehicle(commandArgs[1], player.Position, Rotation.Zero);
                        vehicle.Dimension = player.Dimension;
                        vehicle.EngineOn = true;

                        Task.Run(() =>
                        {
                            Task.Delay(200);
                            player.EmitAsync("warpIntoVehicle", -1);
                        });
                        break;
                    case "wep":
                        if (!Enum.TryParse(commandArgs[1], out WeaponModel wep))
                            return;

                        player.GiveWeapon(wep, 500, true);
                        break;
                    case "time":
                        int hour = int.Parse(commandArgs[1]);

                        if (hour < 0 || hour > 24)
                            return;

                        foreach (IPlayer target in Alt.GetAllPlayers())
                            target.SetDateTime(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hour, 0, 0));
                        break;
                    case "weather":
                        int number = int.Parse(commandArgs[1]);

                        if (number < 0 || number > 13)
                            return;

                        foreach (IPlayer target in Alt.GetAllPlayers())
                            target.SetWeather((WeatherType)number);
                        break;
                    case "go":
                        if (!float.TryParse(commandArgs[1], out float x) || !float.TryParse(commandArgs[2], out float y) ||
                            !float.TryParse(commandArgs[3], out float z))
                            return;

                        player.Position = new Position(x, y, z);
                        break;
                    case "pos":
                        player.Emit("chatmessage", player.Name, $"{player.Position.X} {player.Position.Y} {player.Position.Z} : {player.Rotation.Yaw} : {player.Dimension}");
                        break;
                    case "repair":
                        if (!player.IsInVehicle)
                            return;

                        player.Vehicle.BodyHealth = 1000;
                        player.Vehicle.EngineHealth = 1000;

                        player.Vehicle.SetWheelFixed(0);
                        player.Vehicle.SetWheelFixed(1);
                        player.Vehicle.SetWheelFixed(2);
                        player.Vehicle.SetWheelFixed(3);
                        break;
                    case "vehinfo":
                        if (!player.IsInVehicle)
                            return;

                        player.Emit("chatmessage", player.Name, $"Body Health: {player.Vehicle.BodyHealth}");
                        player.Emit("chatmessage", player.Name, $"Engine Health: {player.Vehicle.EngineHealth}");
                        player.Emit("chatmessage", player.Name, $"Wheel 1: {player.Vehicle.IsWheelBurst(0)}");
                        player.Emit("chatmessage", player.Name, $"Wheel 2: {player.Vehicle.IsWheelBurst(1)}");
                        player.Emit("chatmessage", player.Name, $"Wheel 3: {player.Vehicle.IsWheelBurst(2)}");
                        player.Emit("chatmessage", player.Name, $"Wheel 4: {player.Vehicle.IsWheelBurst(3)}");
                        break;
                }
            }
        }

        private void PlayerConnect(IPlayer player, string reason)
        {
            player.Spawn(new Position(0,0,72));
        }

        public override void OnStop()
        {
            
        }
    }
}
